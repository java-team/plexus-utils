#!/bin/sh -e

TAR=../libplexus-utils_$2.orig.tar.gz
DIR=plexus-utils-$2
TAG=plexus-utils-$2

svn export http://svn.codehaus.org/plexus/plexus-utils/tags/$TAG $DIR
tar -c -z -f $TAR $DIR
rm -rf $DIR ../$TAG

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir
  echo "moved $TAR to $origDir"
fi
